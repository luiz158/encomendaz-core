package com.alienlabz.encomendaz.persistence;

import java.util.List;

import br.gov.frameworkdemoiselle.stereotype.PersistenceController;
import br.gov.frameworkdemoiselle.template.Crud;

import com.alienlabz.encomendaz.domain.Service;

/**
 * Define o comportamento padrão para a persistência de Serviços Postais.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
@PersistenceController
public interface ServicePersistence extends Crud<Service, Long> {

	/**
	 * Obter a lista de serviços que um Serviço Postal possui.
	 * 
	 * @param idPostalService Identificador do Serviço Postal.
	 * @return Lista de serviços.
	 */
	public List<Service> findByPostalService(Long idPostalService);

}
