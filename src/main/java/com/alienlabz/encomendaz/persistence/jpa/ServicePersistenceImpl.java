package com.alienlabz.encomendaz.persistence.jpa;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.Query;

import br.gov.frameworkdemoiselle.annotation.Name;
import br.gov.frameworkdemoiselle.stereotype.PersistenceController;
import br.gov.frameworkdemoiselle.template.JPACrud;
import br.gov.frameworkdemoiselle.util.ResourceBundle;

import com.alienlabz.encomendaz.domain.PostalService;
import com.alienlabz.encomendaz.domain.Service;
import com.alienlabz.encomendaz.persistence.ServicePersistence;

/**
 * Trata da persistência dos dados da entidade {@link PostalService} através de JPA.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
@SuppressWarnings("unchecked")
@PersistenceController
public class ServicePersistenceImpl extends JPACrud<Service, Long> implements ServicePersistence {

	@Inject
	@Name("queries")
	private ResourceBundle queries;

	@Override
	public List<Service> findByPostalService(Long idPostalService) {
		Query query = getEntityManager().createQuery(queries.getString("find.services.by.postalservice"));
		query.setParameter("idPostalService", idPostalService);
		return query.getResultList();
	}

}
