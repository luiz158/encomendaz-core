package com.alienlabz.encomendaz.validator;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.alienlabz.encomendaz.validator.impl.PostalCodeValidator;


/**
 * Anotação de validação para códigos de correios.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
@Documented
@Target({ ElementType.FIELD, ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = PostalCodeValidator.class)
public @interface PostalCode {

	Class<?>[] groups() default {};

	String message() default "";

	Class<? extends Payload>[] payload() default {};

}
