package com.alienlabz.encomendaz.validator.impl;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.alfredlibrary.validadores.Numeros;

import com.alienlabz.encomendaz.validator.PostalCode;


/**
 * Classe de validação, conforme JSR303, para validação de códigos postais.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
public class PostalCodeValidator implements ConstraintValidator<PostalCode, String> {

	@Override
	public void initialize(PostalCode constraintAnnotation) {

	}

	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		return value != null && value.length() == 13 && !Numeros.isNumber(value.substring(0, 2)) && Numeros.isNumber(value.substring(2, 11))
				&& !Numeros.isNumber(value.substring(11, 13));
	}
}
