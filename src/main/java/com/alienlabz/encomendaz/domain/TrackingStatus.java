package com.alienlabz.encomendaz.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;

import org.simpleframework.xml.Default;
import org.simpleframework.xml.DefaultType;
import org.simpleframework.xml.Transient;

/**
 * Classe que representa uma situação específica de uma postagem.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
@Default(value = DefaultType.FIELD, required = false)
@Entity
public class TrackingStatus {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Transient
	private Long id;

	@Column
	private String status;

	@Column
	private String details;

	@Column
	@Temporal(javax.persistence.TemporalType.TIMESTAMP)
	private Date date;

	@Column
	private String place;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getPlace() {
		return place;
	}

	public void setPlace(String place) {
		this.place = place;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getStatus() {
		return status;
	}

	@Override
	public boolean equals(Object obj) {
		boolean result = false;
		if (obj != null) {
			TrackingStatus o = (TrackingStatus) obj;
			if (o.getDetails() == null && this.getDetails() == null) {
				result = true;
			} else {
				result = o.getDetails() != null && o.getDetails().equals(this.getDetails());
			}
			if (o.getPlace() == null && this.getPlace() == null) {
				result = result && true;
			} else {
				result = result && o.getPlace() != null && o.getPlace().equals(this.getPlace());
			}
			if (o.getStatus() == null && this.getStatus() == null) {
				result = result && true;
			} else {
				result = result && o.getStatus() != null && o.getStatus().equals(this.getStatus());
			}
		}
		return result;
	}

}
