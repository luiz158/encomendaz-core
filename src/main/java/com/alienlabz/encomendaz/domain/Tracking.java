package com.alienlabz.encomendaz.domain;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;


import org.hibernate.validator.constraints.NotEmpty;
import org.simpleframework.xml.Default;
import org.simpleframework.xml.DefaultType;
import org.simpleframework.xml.Transient;

import com.alienlabz.encomendaz.validator.PostalCode;

/**
 * Representa uma postagem e seu rastreamento.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
@Default(value = DefaultType.FIELD, required = false)
@Entity
public class Tracking {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Transient
	private Long id;

	@Column
	private boolean taxed;

	@Column
	@Enumerated(EnumType.STRING)
	private State state;

	@Column
	private Long estimative;

	@Column
	private boolean archived;

	@Column
	private boolean notifyToByMail;

	@Column
	private boolean notifyToBySMS;

	@Column
	private boolean notifyToByTwitter;

	@Column
	private boolean notifyFromByMail;

	@Column
	private boolean notifyFromBySMS;

	@Column
	private boolean notifyFromByTwitter;

	@Column
	@Temporal(TemporalType.DATE)
	private Date date;

	@Column
	private String description;

	@Column(unique = true, length = 13)
	@NotNull(message = "{error.tracking.code.incorrect}")
	@NotEmpty(message = "{error.tracking.code.incorrect}")
	@PostalCode(message = "{error.tracking.code.incorrect}")
	private String code;

	@Column
	@Temporal(TemporalType.DATE)
	private Date sent;

	@Column
	private Double value;

	@Column
	private boolean important;

	@Column
	private String details;

	@Column
	private Double weight;

	@Column
	private Double declaredValue;

	@ManyToOne(optional = true, targetEntity = Package.class, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "pack_id")
	private Package pack;

	@ManyToOne(optional = true, targetEntity = Service.class, cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
	@JoinColumn(name = "service_id")
	private Service service;

	@ManyToOne(optional = true, targetEntity = Person.class, cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
	@JoinColumn(name = "from_id", nullable = true, insertable = true, updatable = true)
	private Person from;

	@ManyToOne(optional = true, targetEntity = Person.class, cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
	@JoinColumn(name = "to_id", nullable = true, insertable = true, updatable = true)
	private Person to;

	@OneToMany(orphanRemoval = true, targetEntity = TrackingStatus.class, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "tracking_id", insertable = true, nullable = true, updatable = true)
	private List<TrackingStatus> statuses;

	public TrackingStatus getLastStatus() {
		TrackingStatus status = null;
		if (statuses != null && statuses.iterator().hasNext()) {
			status = statuses.iterator().next();
		}
		return status;
	}

	public TrackingStatus getFirstStatus() {
		TrackingStatus status = null;
		if (statuses != null) {
			for (TrackingStatus s : statuses) {
				status = s;
			}
		}
		return status;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Date getSent() {
		return sent;
	}

	public void setSent(Date sent) {
		this.sent = sent;
	}

	public Double getValue() {
		return value;
	}

	public void setValue(Double value) {
		this.value = value;
	}

	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	public Double getWeight() {
		return weight;
	}

	public void setWeight(Double weight) {
		this.weight = weight;
	}

	public Person getFrom() {
		return from;
	}

	public void setFrom(Person from) {
		this.from = from;
	}

	public Person getTo() {
		return to;
	}

	public void setTo(Person to) {
		this.to = to;
	}

	public List<TrackingStatus> getStatuses() {
		if (statuses == null) {
			statuses = new LinkedList<TrackingStatus>();
		}
		return statuses;
	}

	public void setStatuses(List<TrackingStatus> statuses) {
		this.statuses = statuses;
	}

	public void setImportant(boolean important) {
		this.important = important;
	}

	public boolean isImportant() {
		return important;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Date getDate() {
		return date;
	}

	public void setNotifyToByMail(boolean notifyToByMail) {
		this.notifyToByMail = notifyToByMail;
	}

	public boolean isNotifyToByMail() {
		return notifyToByMail;
	}

	public void setNotifyToBySMS(boolean notifyToBySMS) {
		this.notifyToBySMS = notifyToBySMS;
	}

	public boolean isNotifyToBySMS() {
		return notifyToBySMS;
	}

	public void setNotifyToByTwitter(boolean notifyToByTwitter) {
		this.notifyToByTwitter = notifyToByTwitter;
	}

	public boolean isNotifyToByTwitter() {
		return notifyToByTwitter;
	}

	public void setNotifyFromByMail(boolean notifyFromByMail) {
		this.notifyFromByMail = notifyFromByMail;
	}

	public boolean isNotifyFromByMail() {
		return notifyFromByMail;
	}

	public void setNotifyFromBySMS(boolean notifyFromBySMS) {
		this.notifyFromBySMS = notifyFromBySMS;
	}

	public boolean isNotifyFromBySMS() {
		return notifyFromBySMS;
	}

	public void setNotifyFromByTwitter(boolean notifyFromByTwitter) {
		this.notifyFromByTwitter = notifyFromByTwitter;
	}

	public boolean isNotifyFromByTwitter() {
		return notifyFromByTwitter;
	}

	public void setArchived(boolean archived) {
		this.archived = archived;
	}

	public boolean isArchived() {
		return archived;
	}

	public void setEstimative(Long estimative) {
		this.estimative = estimative;
	}

	public Long getEstimative() {
		return estimative;
	}

	public void setState(State state) {
		this.state = state;
	}

	public State getState() {
		return state;
	}

	/**
	 * Obter a sigla do país de origem da encomenda.
	 * 
	 * @param tracking Encomenda.
	 * @return Sigla do país.
	 */
	public String getCountryAcronym() {
		String result = "unknown";
		if (code != null && code.length() > 4) {
			result = code.substring(code.length() - 2, code.length()).toLowerCase();
		}
		return result;
	}

	public void setTaxed(boolean taxed) {
		this.taxed = taxed;
	}

	public boolean isTaxed() {
		return taxed;
	}

	public void setDeclaredValue(Double declaredValue) {
		this.declaredValue = declaredValue;
	}

	public Double getDeclaredValue() {
		return declaredValue;
	}

	public void setPack(Package pack) {
		this.pack = pack;
	}

	public Package getPack() {
		return pack;
	}

	public void setService(Service service) {
		this.service = service;
	}

	public Service getService() {
		return service;
	}

	public String getSearchable() {
		return code + " " + sent.toString() + " " + (from != null ? from.getName() : "") + " "
				+ (to != null ? to.getName() : "") + " " + description;
	}

	public boolean isDelivered() {
		return (this.state != null && this.state == State.Delivered);
	}
}
