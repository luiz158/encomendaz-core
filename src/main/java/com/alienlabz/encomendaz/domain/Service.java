package com.alienlabz.encomendaz.domain;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.simpleframework.xml.Default;
import org.simpleframework.xml.DefaultType;

/**
 * Um serviço prestado por um serviço postal.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
@Default(value = DefaultType.FIELD, required = false)
@Entity
public class Service {

	@Id
	private Long id;

	@Column
	private boolean active;

	@ManyToOne(optional = true, targetEntity = PostalService.class, cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
	@JoinColumn(name = "postalservice_id")
	private PostalService postalService;

	@Column
	private String name;

	@Column
	private String externalCode;

	public Service() {

	}

	public Service(Long id, String name, PostalService postalService, String externalCode) {
		this.id = id;
		this.name = name;
		this.postalService = postalService;
		this.externalCode = externalCode;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setPostalService(PostalService postalService) {
		this.postalService = postalService;
	}

	public PostalService getPostalService() {
		return postalService;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public boolean isActive() {
		return active;
	}

	@Override
	public String toString() {
		return name;
	}

	public void setExternalCode(String externalCode) {
		this.externalCode = externalCode;
	}

	public String getExternalCode() {
		return externalCode;
	}

}
