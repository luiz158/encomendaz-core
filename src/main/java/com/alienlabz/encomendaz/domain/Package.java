package com.alienlabz.encomendaz.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.simpleframework.xml.Default;
import org.simpleframework.xml.DefaultType;
import org.simpleframework.xml.Transient;

/**
 * Pacotes para envio de encomendas.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
@Default(value = DefaultType.FIELD, required = false)
@Entity
public class Package {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Transient
	private Long id;

	@Column
	private int altitude;

	@Column
	private int breadth;

	@Column
	private int length;

	@Column
	private int girth;
	
	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public int getLength() {
		return length;
	}

	public void setGirth(int girth) {
		this.girth = girth;
	}

	public int getGirth() {
		return girth;
	}

	public void setAltitude(int altitude) {
		this.altitude = altitude;
	}

	public int getAltitude() {
		return altitude;
	}

	public void setBreadth(int breadth) {
		this.breadth = breadth;
	}

	public int getBreadth() {
		return breadth;
	}
}
