package com.alienlabz.encomendaz.util;

import br.gov.frameworkdemoiselle.util.Strings;

/**
 * Utilitário genérico para tratar o estado de objetos.
 * 
 * @author Marlon Silva Carvalho
 *
 */
final public class ObjectUtil {

	/**
	 * Verificar se existe um objeto nulo ou uma String vazia nos parâmetros.
	 * 
	 * @param objects Objetos que serão verificados.
	 * @return Verdadeiro caso algum objeto seja nulo ou uma string vazia.
	 */
	public static boolean isNullOrEmpty(Object... objects) {
		boolean result = false;
		for (Object object : objects) {
			if (object == null) {
				result = true;
				break;
			} else {
				if (object instanceof String) {
					String str = (String) object;
					if (Strings.isEmpty(str)) {
						result = true;
						break;
					}
				}
			}
		}
		return result;
	}
}
