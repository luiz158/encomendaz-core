package com.alienlabz.encomendaz.util;

/**
 * Agrupador de dados extras necessários para fazer consultas.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
final public class SearchBundle {
	private String searchText;

	public SearchBundle() {
		this("");
	}

	public SearchBundle(String text) {
		this.setSearchText(text);
	}

	public void setSearchText(String searchText) {
		this.searchText = searchText;
	}

	public String getSearchText() {
		if(searchText != null) {
			searchText = searchText.toLowerCase();
		}
		return searchText;
	}
	
}
