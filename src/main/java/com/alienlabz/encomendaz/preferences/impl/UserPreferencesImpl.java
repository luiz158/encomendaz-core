package com.alienlabz.encomendaz.preferences.impl;

import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

import javax.inject.Inject;

import org.jasypt.util.text.BasicTextEncryptor;

import com.alienlabz.encomendaz.domain.EmailServerSecurity;
import com.alienlabz.encomendaz.preferences.StorePreferencesException;
import com.alienlabz.encomendaz.preferences.UserPreferences;
import com.alienlabz.encomendaz.util.NumberUtil;

import br.gov.frameworkdemoiselle.annotation.Name;
import br.gov.frameworkdemoiselle.stereotype.BusinessController;
import br.gov.frameworkdemoiselle.util.ResourceBundle;
import br.gov.frameworkdemoiselle.util.Strings;

/**
 * Trata da persistência das preferências do usuário usando a Preferences API nativa do Java.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
@BusinessController
public class UserPreferencesImpl implements UserPreferences {

	@Inject
	@Name("system-core")
	private ResourceBundle bundleSystem;

	private Preferences preferences;

	public UserPreferencesImpl() {
		setPreferences(Preferences.userNodeForPackage(UserPreferences.class));
	}

	@Override
	public String getNotificationSound() {
		return getPreferences().get(bundleSystem.getString("default.notification.sound.key"), "");
	}

	@Override
	public void setNotificationSound(String sound) {
		getPreferences().put(bundleSystem.getString("default.notification.sound.key"), sound);
	}

	@Override
	public int getRefreshRate() {
		return getPreferences().getInt(bundleSystem.getString("default.refresh.rate.key"),
				Integer.valueOf(bundleSystem.getString("default.refresh.rate")));
	}

	@Override
	public void setRefreshRate(int refreshRate) {
		getPreferences().putInt(bundleSystem.getString("default.refresh.rate.key"), refreshRate);
	}

	@Override
	public String getEmailServer() {
		return getPreferences().get(bundleSystem.getString("default.email.server.key"),
				(bundleSystem.getString("default.email.server")));
	}

	@Override
	public void setEmailServer(String emailServer) {
		if (emailServer != null) {
			getPreferences().put(bundleSystem.getString("default.email.server.key"), emailServer);
		}
	}

	@Override
	public int getEmailServerPort() {
		return getPreferences().getInt(bundleSystem.getString("default.email.server.port.key"),
				NumberUtil.toInteger(bundleSystem.getString("default.email.server.port")));
	}

	@Override
	public void setEmailServerPort(int emailServerPort) {
		getPreferences().putInt(bundleSystem.getString("default.email.server.port.key"), emailServerPort);
	}

	@Override
	public String getEmailUsername() {
		return getPreferences().get(bundleSystem.getString("default.email.server.username.key"),
				(bundleSystem.getString("default.email.server.username")));
	}

	@Override
	public void setEmailUsername(String username) {
		if (username != null) {
			getPreferences().put(bundleSystem.getString("default.email.server.username.key"),
					(username != null ? username : ""));
		}
	}

	@Override
	public String getEmailPassword() {
		String result = "";
		BasicTextEncryptor textEncryptor = new BasicTextEncryptor();
		textEncryptor.setPassword("FIGHTTHEFUTURE");
		String pass = getPreferences().get(bundleSystem.getString("default.email.server.password.key"), null);
		if (!Strings.isEmpty(pass)) {
			result = textEncryptor.decrypt(pass);
		}
		return result;
	}

	@Override
	public void setEmailPassword(String password) {
		if (password != null) {
			BasicTextEncryptor textEncryptor = new BasicTextEncryptor();
			textEncryptor.setPassword("FIGHTTHEFUTURE");
			getPreferences().put(bundleSystem.getString("default.email.server.password.key"),
					textEncryptor.encrypt(password));
		}
	}

	@Override
	public EmailServerSecurity getEmailSecurity() {
		String sec = getPreferences().get(bundleSystem.getString("default.email.server.security.key"),
				(bundleSystem.getString("default.email.server.security")));
		if ("".equals(sec)) {
			return Enum.valueOf(EmailServerSecurity.class, bundleSystem.getString("default.email.server.security"));
		}
		return Enum.valueOf(EmailServerSecurity.class, sec);
	}

	@Override
	public void setEmailSecurity(EmailServerSecurity security) {
		if (security != null) {
			getPreferences().put(bundleSystem.getString("default.email.server.security.key"), security.getType());
		}
	}

	@Override
	public String getEmailTemplate() {
		return getPreferences().get(bundleSystem.getString("default.email.template.key"),
				(bundleSystem.getString("default.email.template")));
	}

	@Override
	public void setEmailTemplate(String template) {
		if (template != null) {
			getPreferences().put(bundleSystem.getString("default.email.template.key"), template);
		}
	}

	@Override
	public void setUseProxy(boolean use) {
		getPreferences().putBoolean(bundleSystem.getString("default.use.proxy.key"), use);
	}

	@Override
	public String getProxyUsername() {
		return getPreferences().get(bundleSystem.getString("default.proxy.username.key"),
				bundleSystem.getString("default.proxy.username"));
	}

	@Override
	public void setProxyUsername(String username) {
		if (username != null) {
			getPreferences().put(bundleSystem.getString("default.proxy.username.key"), username);
		}
	}

	@Override
	public void store() {
		try {
			getPreferences().flush();
		} catch (BackingStoreException e) {
			throw new StorePreferencesException();
		}
	}

	@Override
	public void setEmailSender(String sender) {
		if (sender != null) {
			getPreferences().put(bundleSystem.getString("default.email.sender.key"), sender);
		}
	}

	@Override
	public String getEmailSender() {
		return getPreferences().get(bundleSystem.getString("default.email.sender.key"),
				bundleSystem.getString("default.email.sender"));
	}

	@Override
	public String getEmailSubject() {
		return getPreferences().get(bundleSystem.getString("default.email.subject.key"),
				bundleSystem.getString("default.email.subject"));
	}

	@Override
	public void setEmailSubject(String subject) {
		if (subject != null) {
			getPreferences().put(bundleSystem.getString("default.email.subject.key"), subject);
		}
	}

	@Override
	public String getDefaultSkin() {
		return getPreferences().get(bundleSystem.getString("default.visual.skin.key"),
				bundleSystem.getString("default.visual.skin"));
	}

	@Override
	public void setDefaultSkin(String skin) {
		if (skin != null) {
			getPreferences().put(bundleSystem.getString("default.visual.skin.key"), skin);
		}
	}

	@Override
	public boolean isUseProxy() {
		return getPreferences().getBoolean(bundleSystem.getString("default.use.proxy.key"),
				Boolean.valueOf(bundleSystem.getString("default.use.proxy")));
	}

	@Override
	public String getProxyPassword() {
		String result = "";
		BasicTextEncryptor textEncryptor = new BasicTextEncryptor();
		textEncryptor.setPassword("FIGHTTHEFUTURE");
		String pass = getPreferences().get(bundleSystem.getString("default.network.proxy.password.key"), "");
		if (!Strings.isEmpty(pass)) {
			result = textEncryptor.decrypt(pass);
		}
		return result;
	}

	@Override
	public void setProxyPassword(String pas) {
		if (pas != null) {
			BasicTextEncryptor textEncryptor = new BasicTextEncryptor();
			textEncryptor.setPassword("FIGHTTHEFUTURE");
			getPreferences().put(bundleSystem.getString("default.network.proxy.password.key"),
					textEncryptor.encrypt(pas));
		}
	}

	public void setPreferences(Preferences preferences) {
		this.preferences = preferences;
	}

	public Preferences getPreferences() {
		return preferences;
	}

	@Override
	public Long getDefaultFrom() {
		return getPreferences().getLong(bundleSystem.getString("default.from.key"), 0);
	}

	@Override
	public void setDefaultFrom(Long id) {
		getPreferences().putLong(bundleSystem.getString("default.from.key"), id);
	}

	@Override
	public Long getDefaultTo() {
		return getPreferences().getLong(bundleSystem.getString("default.to.key"), 0);
	}

	@Override
	public void setDefaultTo(Long id) {
		getPreferences().putLong(bundleSystem.getString("default.to.key"), id);
	}

	@Override
	public boolean isSendAutomaticEmails() {
		return getPreferences().getBoolean(bundleSystem.getString("default.send.automatic.emails.key"), false);
	}

	@Override
	public void setSendAutomaticEmails(boolean send) {
		getPreferences().putBoolean(bundleSystem.getString("default.send.automatic.emails.key"), send);
	}

}
