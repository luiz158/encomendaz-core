package com.alienlabz.encomendaz.preferences;

/**
 * Exceção lançada quando ocorre um erro ao salvar as preferências do usuário.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
public class StorePreferencesException extends RuntimeException {

}
